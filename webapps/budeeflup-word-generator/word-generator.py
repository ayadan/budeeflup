import random

vowelSounds = [ "a", "e", "i", "o", "u", "ee", "oo", "ai", "ei", "oi" ]
consonantSounds = [ "b", "d", "f", "g", "h", "j", "k", "l", "m", "n", "p", "r", "s", "t", "v", "w", "z", "sh", "ch", "pt", "kr", "br", "dr" ]

def GetVowel():
    global vowelSounds
    index = random.randint( 0, len( vowelSounds ) - 1 )
    return vowelSounds[ index ]

def GetConsonant():
    global consonantSounds
    index = random.randint( 0, len( consonantSounds ) - 1 )
    return consonantSounds[ index ]

def GetWord():
    parts = random.randint( 1, 8 )
    word = ""

    for i in range( parts ):
        vorc = random.randint( 1, 2 )

        if ( vorc == 1 ):
            word += GetVowel()

        elif ( vorc == 2 ):
            word += GetConsonant()

    return word

print( "BUDEEFLUP WORD SUGGESTER" )

while ( True ):
    for i in range( 10 ):
        print( "\t" + GetWord() + "\t" + GetWord() + "\t" + GetWord() )

    print( "\n" )
    again = raw_input( "Again? (y/n): " )

    if ( again == "n" ):
        break;

    print( "\n" )
