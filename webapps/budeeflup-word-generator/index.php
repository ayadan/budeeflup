
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="favicon.png">

    <title>Budeeflup Word Suggestor - By Rachel Singh</title>
    <script src="content/jquery-2.2.4/jquery-2.2.4.min.js"></script>
        
    <link href="content/bootstrap-3.3.6-dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- <link href="content/bootstrap-3.3.6-dist/css/ie10-viewport-bug-workaround.css" rel="stylesheet"> -->
    <!-- <link href="content/jumbotron-narrow.css" rel="stylesheet"> -->
    <!-- <script src="content/bootstrap-3.3.6-dist/js/ie-emulation-modes-warning.js"></script> -->

    <style type="text/css">
        .breakdown { background: #c9e1ff; }
        .unofficial { background: #ffc9db; }

        .breakdown, .unofficial { font-size: 0.9em; border-radius: 5px; text-align: left; padding: 5px; font-weight: bold; }
    </style>
  </head>

  <body>

    <? include_once( "backend.php" ); ?>
    
    <script type="text/javascript">
        $( function() {
            
            vowelSounds = [ "a", "e", "i", "o", "u", "ee", "oo", "ai", "ei", "oi", "ay", "ya" ];
            consonantSounds = [ "b", "d", "f", "g", "h", "j", "k", "l", "m", "n", "p", "r", "s", "t", "v", "w", "z", "sh", "ch", "pt", "kr", "br", "dr" ];

            function GetVowel()
            {
                var v = Math.floor( Math.random() * vowelSounds.length );
                return vowelSounds[v];
            }

            function GetConsonant()
            {
                var c = Math.floor( Math.random() * consonantSounds.length );
                return consonantSounds[c];
            }

            function GenerateWords()
            {
                var parts = Math.floor( Math.random() * 4 ) + 2;
                var word = "";
                
                for ( var i = 0; i < parts; i++ )
                {
                    var type = Math.floor( Math.random() * 2 );
                    if ( type == 0 )
                    {
                        word += GetVowel();
                    }
                    else if ( type == 1 )
                    {
                        word += GetConsonant();
                    }
                }

                $( "#word-list" ).append( "<li>" + word + "</li>" );
            }
            
            $( "#generateButton" ).click( function() {
                for ( var i = 0; i < 20; i++ )
                {
                    GenerateWords();
                }
            } ); // click

            for ( var i = 0; i < 20; i++ )
            {
                GenerateWords();
            }
            
        } ); // ready
        
        
    </script>

    <div class="container">
      <div class="header clearfix">
        <nav>
          
        </nav>
        <h3 class="text-muted">Budeeflup Word Suggestor</h3>
      </div>

      <div class="jumbotron">
        <div class="row">
            <div class="col-md-12">
                <p><a class="btn btn-lg btn-success pull-right btn-block" href="#" role="button" id="generateButton">Generate more</a></p>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-12">
                <ul id="word-list">
                </ul>
            </div>
        </div>
      </div>

    

      <footer class="footer">
        <p>Programmed by Rachel Wil Sha Singh (Rachel@Moosader.com)</p>
        <p>Dictionary from <a href="https://bitbucket.org/ayadan/budeeflup/src">Budeeflup Language</a></p>
      </footer>

    </div> <!-- /container -->


  </body>
</html>
