# Budeeflup Grammar Reference

## Sounds

Sounds will be fleshed out as more people use the lanuage.

## Pronouns

* *apt* - me, singular first person
* *eis* - you, singular second person
* *de* - they/it, singular third person
* *aptex* - we, inclusive plural first person
* *aptex nobeis* - we, exclusive plural first person

## Who/What/When/Where...

* People/things...
    * *upo* - Which person/thing; who, what
    * *dapo* - That person/thing; that
    * *lapo* - This person/thing; this
    * *apo* - Any person/thing; anyone, anything
    * *alpo* - Every person/thing; everyone, everything
    * *nopo* - No person/thing; nobody, nothing
* Places...
    * *ulok* - Which place; where
    * *dalok* - That place; there
    * *lalok* - This place; here
    * *alok* - Any place; anywhere
    * *alalo* - Every place; everywhere
    * *nolok* - No place; nowhere
* Times...
    * *uampam* - Which time; when
    * *dampam* - That time; then
    * *lampam-* - This time; now
    * *amapama* - Any time
    * *alamapam* - Every time; forever
    * *nampam* - No time; never

## "NOUN is NOUN" sentences

**ORDER: [thing that subject is] arbee [subject] **

* *"Toyg arbee apt."*
    * English: I am mayor.
    * *apt* - me
    * *arbee* - to be
    * *toyg* - mayor

## "NOUN VERBs" sentences

**ORDER: [tense]-[verb: thing subject is doing] [subject]**

* *"Krakltoop apt."*
    * English: I am pooping.
    * *apt* - me
    * *toop* - to poop
    * *krakl-* - present tense

## "SUBJECT VERBs OBJECT" sentences

**ORDER: [object] [tense]-[verb] [subject]**

* *"Eis krakl-ai apt."*
    * English: I see you.
    * *ai* - to see
    * *krakl-* - present tense
    * *apt* - me
    * *eis* - you

## "NOUN VERBS VERB" sentences

**ORDER: [verb: primary] [verb: secondary] [subject]**

* *"Wana toop apt."*
    * English: I want to poop
    * *apt* - me
    * *toop* - to poop
    * *wana* - to want

## General notes

* OVS (Object-Verb-Subject) order as a default, use *"-boo"* suffix on object if not in this order.
* Question indicator: Put *"Tater"* at beginning of sentence.
    * This is not required for question words like "Who", "When", etc, but you can add it if you want.
* Non-gendered.
* Plurality marker is optional. Should be noted based on context, but if you want to be really specific:
    * *"-x"* (following vowel) or *"-ex"* (following consonant) marks plurality on a noun.
    * to be pronounced like s, but you put the tip of your tongue on the roof of your mouth and force the air around the sides of it, through your molars
* The word for "not" is *"Nob"*, and can be prefixed to nouns and verbs. The "b" can be dropped if following letter is a consonant.


# Example phrases

* Toyg krakl-arbee apt. - I am mayor (presently).
* Aptmee budee - My friend
* Aptmee budee arbee eis. - You are my friend.

* Tater eis arbee upo? - Who are you?
* Tater de arbee upo? - What is that?

* Krakl-toop nopo. - Nobody is pooping right now.
